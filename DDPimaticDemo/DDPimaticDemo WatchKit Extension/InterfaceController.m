//
//  InterfaceController.m
//  DDPimaticWatch WatchKit Extension
//
//  Created by Dirk Fischbach on 14.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>

#import "InterfaceController.h"
#import "RowDevice.h"
#import "RowDeviceSwitch.h"

@interface InterfaceController()
{
    NSArray *_devices;
}

@property (weak, nonatomic) IBOutlet WKInterfaceTable *table;

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context
{
    [super awakeWithContext:context];

    // listen for notifications
    [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceDeviceListChangedNotification  object:nil queue:nil usingBlock:^(NSNotification *notification) {
        [self _updateDevices];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceDeviceChangedNotification  object:nil queue:nil usingBlock:^(NSNotification *notification) {
        [self _updateDevice:notification.pimaticServiceChangedObject];
    }];
}

- (void)willActivate
{
    [super willActivate];
    
    // connect
    DDPimaticService *service = [DDPimaticService sharedInstance];
    service.serverURL = [NSURL URLWithString:@"http://demo.pimatic.org"];
    
    [service loginWithUsername:@"demo" password:@"demo" completion:^(id result, NSError *error) {
        NSLog(@"Login to %@ with result %@", service.serverURL, error);
    }];
}

- (void)didDeactivate
{
    // disconnect
    [[DDPimaticService sharedInstance] logoutWithCompletion:nil];
    
    [super didDeactivate];
}


#pragma mark - Internal

- (void) _updateDevices
{
    NSArray *knownTypes = @[@"device", @"switch"];

    [self.table setRowTypes:knownTypes];

    // get devices from service
    _devices = [DDPimaticService sharedInstance].devices;

    
    // add supported devices ( template 'device' or 'switch') to the table
    NSUInteger insertIndex = 0;
    for (int i=0; i<_devices.count; i++)
    {
        DDPimaticDevice *device = _devices[i];
        
        if ( ![knownTypes containsObject:device.templateName] ) {
            continue;
        }
        
        [self.table insertRowsAtIndexes:[NSIndexSet indexSetWithIndex:insertIndex] withRowType:device.templateName];

        RowDevice *row = [self.table rowControllerAtIndex:insertIndex++];
        row.device = device;
    }
}


- (void) _updateDevice:(DDPimaticDevice*)device
{
    // update a single device on value changed
    for (NSUInteger i=0; i<self.table.numberOfRows; i++) {
        RowDevice *rowDevice = [self.table rowControllerAtIndex:i];
        if ( [rowDevice.device.uid isEqualToString:device.uid] ) {
            rowDevice.device = device;
        }
    }
}

@end



