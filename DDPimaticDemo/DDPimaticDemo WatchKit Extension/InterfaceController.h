//
//  InterfaceController.h
//  DDPimaticWatch WatchKit Extension
//
//  Created by Dirk Fischbach on 14.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
