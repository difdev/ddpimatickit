//
//  TableRowDevice.m
//  DDPimaticWatch WatchKit Extension
//
//  Created by Dirk Fischbach on 14.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//


#import "RowDevice.h"

@implementation RowDevice

- (void) setDevice:(DDPimaticDevice *)device
{
    _device = device;
    
    [self updateUI];
}


- (void) updateUI
{
    [self.labelDeviceName setText:_device.name];
    DDPimaticDeviceAttribute *attribute = _device.attributes.firstObject;
    
    NSString *textValue = nil;
    if ( attribute != nil ) {
        if ( attribute.name.length > 0 && _device.attributes.count > 1) {
            [self.labelValueName setText:attribute.name];
        } else  {
            [self.labelValueName setText:nil];
        }
        
        if ( attribute.unit.length > 0 ) {
            textValue = [NSString stringWithFormat:@"%@ %@", attribute.value, attribute.unit];
        } else {
            textValue = [NSString stringWithFormat:@"%@", attribute.value];
        }
    }
    [self.labelValue setText:textValue];
}

@end
