//
//  RowDeviceSwitch.h
//  DDPimaticWatch WatchKit Extension
//
//  Created by Dirk Fischbach on 15.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

#import "RowDevice.h"

@interface RowDeviceSwitch : RowDevice

@property (weak, nonatomic) IBOutlet WKInterfaceSwitch *valueSwitch;

@end
