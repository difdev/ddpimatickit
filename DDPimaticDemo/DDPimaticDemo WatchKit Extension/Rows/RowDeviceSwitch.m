//
//  RowDeviceSwitch.m
//  DDPimaticWatch WatchKit Extension
//
//  Created by Dirk Fischbach on 15.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "RowDeviceSwitch.h"

@implementation RowDeviceSwitch


- (void) updateUI
{
    [self.labelDeviceName setText:self.device.name];
    
    DDPimaticDeviceAttribute *attribute = [self.device.attributes deviceAttributeWithName:@"state"];
    if ( attribute != nil )
    {
        if ( [attribute.value isKindOfClass:[NSNumber class]] ) {
            [self.valueSwitch setOn:((NSNumber*)attribute.value).boolValue];
        }
    }
}

#pragma mark - Action handler

- (IBAction)actionSwitch:(BOOL)on
{
    DDPimaticDeviceAction *action = [self.device.actions deviceActionWithName:on ? @"turnOn" : @"turnOff" ];
    if ( action != nil )
    {
        DDPimaticService *service = [DDPimaticService sharedInstance];
        
        [service executeDeviceAction:action deviceUID:self.device.uid params:nil completion:^(id result, NSError *error) {
            NSLog(@"Exectuted action %@ with result %@", action.name, error);
        }];
    }
    
}

@end
