//
//  TableRowDevice.h
//  DDPimaticWatch WatchKit Extension
//
//  Created by Dirk Fischbach on 14.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>
#import <DDPimaticKit/DDpimaticKit.h>


@interface RowDevice : NSObject

@property (weak, nonatomic) IBOutlet WKInterfaceLabel *labelDeviceName;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *labelValueName;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *labelValue;

@property (nonatomic, strong) DDPimaticDevice *device;

- (void) updateUI;

@end
