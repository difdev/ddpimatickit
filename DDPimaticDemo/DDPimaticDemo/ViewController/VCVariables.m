//
//  VCVariables.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 17.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "VCVariables.h"

@interface VCVariables ()
{
    NSArray         *_variables;
    NSMutableArray  *_observers;
}
@end

@implementation VCVariables

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Variables";
    
    _observers = [NSMutableArray new];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_observers addObject: [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceVariableListChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _updateVariables];
    }]];

    [_observers addObject: [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceVariableChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _updateVariable:[note pimaticServiceChangedObject]];
    }]];

    
    [self _updateVariables];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    for (id observer in _observers) {
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
    }
    [_observers removeAllObjects];
}


#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _variables.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    DDPimaticVariable *variable = _variables[indexPath.row];
    
    NSLog(@"variable %@: %@", variable.name, variable.value);
    cell.textLabel.text         = variable.name;
    cell.detailTextLabel.text   = ((NSObject*)variable.value).description;
    
    return cell;
}

#pragma mark - Internal

- (void) _updateVariables
{
    _variables = [DDPimaticService sharedInstance].variables;
    [self.tableView reloadData];
}

- (void) _updateVariable:(DDPimaticVariable*)variable
{
    NSUInteger index = [_variables indexOfObject:variable];
    if ( index != NSNotFound ) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}


@end
