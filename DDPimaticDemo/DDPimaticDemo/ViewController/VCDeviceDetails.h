//
//  VCDeviceDetails.h
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DDPimaticKit/DDPimaticKit.h>
#import "VCBase.h"


@interface VCDeviceDetails : VCBase

@property (nonatomic, strong) DDPimaticDevice *device;

@end
