//
//  VCUser.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 24.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "VCUser.h"

@interface VCUser ()
{
    id              _observerUserInfo;
    NSDictionary    *_jsonPermissions;
}

@property (weak, nonatomic) IBOutlet UIButton *buttonLogout;

@property (nonatomic, strong) DDPimaticUserInfo *userInfo;

@end


@implementation VCUser

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _observerUserInfo = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceUserInfoChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _updateState];
    }];
    [self _updateState];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_observerUserInfo];
}

#pragma mark - Action handler

- (IBAction)actionLogout:(id)sender
{
    [[DDPimaticService sharedInstance] logoutWithCompletion:^(id result, NSError *error) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}


#pragma mark - TableView datasource

- (NSInteger) numberOfSectionsInTableView:(UITableView*)tableView
{
    return (self.userInfo != nil) ? 2 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            // properties
            return 2;
            break;
        case 1:
            // permissions
            return _jsonPermissions.count;
            break;
        default:
            break;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSString *title     = nil;
    NSString *detail    = nil;
    
    if ( indexPath.section == 0 )
    {
        // Properties
        switch (indexPath.row) {
            case 0:
                title   = @"Username";
                detail  = self.userInfo.username;
                break;
            case 1:
                title   = @"Role";
                detail  = self.userInfo.role;
                break;
            default:
                break;
        }
    }
    else if ( indexPath.section == 1 )
    {
        // permissions
        title   = _jsonPermissions.allKeys[indexPath.row];
        detail  = ((NSObject*)_jsonPermissions[title]).description;
    }
    
    cell.textLabel.text         = title;
    cell.detailTextLabel.text   = detail;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ( section == 1 ) {
        return @"Permissions";
    }
    return nil;
}


- (void) _updateState
{
    DDPimaticService *service = [DDPimaticService sharedInstance];
    
    self.userInfo = service.userInfo;
    _jsonPermissions = self.userInfo.permissions.toJSON;
    
    self.buttonLogout.enabled = (self.userInfo != nil);
    
    [self.tableView reloadData];
}



@end
