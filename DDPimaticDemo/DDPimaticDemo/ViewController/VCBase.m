//
//  VCBase.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 18.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "VCBase.h"

@interface VCBase ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation VCBase

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.tableView.dataSource   = self;
    self.tableView.delegate     = self;
}

#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

@end
