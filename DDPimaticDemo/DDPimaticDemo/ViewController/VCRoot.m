//
//  VCRoot.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 17.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "Constants.h"
#import "VCLogin.h"
#import "VCRoot.h"

@interface VCRoot ()
{
    NSArray     *_dataSource;
    NSArray     *_dataSourceLogin;
    NSArray     *_dataSourceLogout;
    
}
@end

@implementation VCRoot

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"pimatic Demo";
    
    _dataSourceLogin = @[
        @[@"User", @"VCUser"],
        @[@"Devices", @"VCDevices"],
        @[@"Variables", @"VCVariables"],
        @[@"Groups", @"VCGroups"],
        @[@"Pages", @"VCPages"],
        @[@"Rules", @"VCRules"]
    ];
    _dataSourceLogout = @[
                         @[@"Login", @"VCLogin"]
                         ];

    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VCLogin"];
    [self.navigationController pushViewController:vc animated:NO];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    DDPimaticService *service = [DDPimaticService sharedInstance];
    _dataSource = service.isLoggedin ? _dataSourceLogin : _dataSourceLogout;
    [self.tableView reloadData];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    NSArray *item = _dataSource[indexPath.row];
    
    cell.textLabel.text = item[0];
    cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableView delegate selection

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *item = _dataSource[indexPath.row];

    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:item[1]];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
