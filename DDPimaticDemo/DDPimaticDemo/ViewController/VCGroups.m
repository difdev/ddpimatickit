//
//  VCGroups.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 28.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "VCGroups.h"

@interface VCGroups ()
{
    NSArray *_groups;
    id      _observer;
}

@end

@implementation VCGroups

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Groups";
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceGroupListChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _updateGroups:note.pimaticServiceChangedListDescription];
    }];
    [self _updateGroups:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}


#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _groups.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    DDPimaticGroup *group = _groups[indexPath.row];
    
    cell.textLabel.text         = group.name;
    
    return cell;
}

#pragma mark - Internal

- (void) _updateGroups:(DDPimaticListChangedDescription*)description
{
    if ( description == nil )
    {
        _groups = [DDPimaticService sharedInstance].groups;
        [self.tableView reloadData];
    }
    else
    {
        _groups = description.list;
        switch (description.changedType) {
            case DDPimaticListChangedTypeInsertion:
                [self.tableView insertRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeRemoval:
                [self.tableView deleteRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeReplacement:
                [self.tableView reloadRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeReset:
            default:
                [self.tableView reloadData];
                break;
        }
    }
}

@end
