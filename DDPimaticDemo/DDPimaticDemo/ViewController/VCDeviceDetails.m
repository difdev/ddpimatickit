//
//  VCDeviceDetails.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "CellDeviceAction.h"
#import "VCDeviceDetails.h"

@implementation VCDeviceDetails
{
    id _observer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CellDeviceAction" bundle:nil] forCellReuseIdentifier:@"cellaction"];
    
    self.tableView.estimatedRowHeight   = 130;
    self.tableView.rowHeight            = UITableViewAutomaticDimension;

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceDeviceChangedNotification  object:nil queue:nil usingBlock:^(NSNotification *note) {
        DDPimaticDevice *device = note.userInfo[DDPimaticObjectChangedKey];
        if ( [device isEqual:self.device] ) {
            [self _updateDevice];
        }
    }];
    [self _updateDevice];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}


- (void) setDevice:(DDPimaticDevice *)device
{
    _device = device;
    [self _updateDevice];
}

#pragma mark - TableView datasource

- (NSInteger) numberOfSectionsInTableView:(UITableView*)tableView
{
    return (self.device != nil) ? 3 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            // properties
            return 2;
            break;
        case 1:
            // attributes
            return self.device.attributes.count;
            break;
        case 2:
            // actions
            return self.device.actions.count;
            break;
        default:
            break;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title     = nil;
    NSString *detail    = nil;
    
    if ( indexPath.section == 0 )
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

        // Properties
        switch (indexPath.row) {
            case 0:
                title   = @"Name";
                detail  = self.device.name;
                break;
            case 1:
                title   = @"Template";
                detail  = self.device.templateName;
                break;
            default:
                break;
        }
        cell.textLabel.text         = title;
        cell.detailTextLabel.text   = detail;
        return cell;
    }
    else if ( indexPath.section == 1 )
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        // attributes
        DDPimaticDeviceAttribute *attribute = self.device.attributes[indexPath.row];
        title   = attribute.name;
        detail  = ((NSObject*)attribute.value).description;

        cell.textLabel.text         = title;
        cell.detailTextLabel.text   = detail;
        
        return cell;
    }
    else if ( indexPath.section == 2 )
    {
        CellDeviceAction *cell = [tableView dequeueReusableCellWithIdentifier:@"cellaction"];
        
        // actions
        DDPimaticDeviceAction *action = self.device.actions[indexPath.row];
        title   = action.name;
        detail  = action.actionDescription;
        
        NSLog(@"Action %@\n  params %@\n  returns %@\n", action.name, action.params, action.returns);
        
        cell.labelTitle.text        = action.name;
        cell.labelDescription.text  = action.actionDescription;
        cell.labelParams.text       = action.params.description;
        cell.labelReturns.text      = action.returns.description;
        return cell;
    }
    
    return nil;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Properties";
            break;
        case 1:
            return @"Attributes";
            break;
        case 2:
            return @"Actions";
            break;
        default:
            break;
    }
    return nil;
}

#pragma mark - UITableView delegate selection

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ( indexPath.section == 2 )
    {
        DDPimaticDeviceAction *action = self.device.actions[indexPath.row];
        
        [[DDPimaticService sharedInstance] executeDeviceAction:action deviceUID:self.device.uid params:nil completion:^(id result, NSError *error) {
            NSLog(@"executeAction result: %@  error: %@", result, error);
            
            UIAlertController *alert = nil;
            // return value ?
            if ( error != nil || action.returns.count > 0 )
            {
                if ( error != nil ) {
                    alert = [UIAlertController
                                alertControllerWithTitle:@"Error"
                                message:error.description
                                preferredStyle:UIAlertControllerStyleAlert];
                } else {
                    alert = [UIAlertController
                              alertControllerWithTitle:@"Result"
                              message:((NSObject*)result).description
                              preferredStyle:UIAlertControllerStyleAlert];
                }

                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         [self dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }
}


#pragma mark - Internal

- (void) _updateDevice
{
    self.title  = self.device.name;
    
    [self.tableView reloadData];
}

@end
