//
//  VCRules.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "VCRules.h"

@interface VCRules ()
{
    NSArray *_rules;
    id      _observer;
}

@end

@implementation VCRules

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Rules";
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceRuleListChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _updateRules];
    }];
    [self _updateRules];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}


#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _rules.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    DDPimaticRule *rule = _rules[indexPath.row];
    
    cell.textLabel.text         = rule.name;
    cell.detailTextLabel.text   = rule.tokenString;
    
    return cell;
}

#pragma mark - Internal

- (void) _updateRules
{
    _rules = [DDPimaticService sharedInstance].rules;
    [self.tableView reloadData];
}


@end
