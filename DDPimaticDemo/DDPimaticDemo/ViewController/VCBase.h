//
//  VCBase.h
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 18.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Base class of the for a view controller with tableview.
 */
@interface VCBase : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
