//
//  VCPages.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 28.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "VCPages.h"

@interface VCPages ()
{
    NSArray *_pages;
    id      _observer;
}

@end

@implementation VCPages

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Pages";
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServicePageListChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _updatePages:note.pimaticServiceChangedListDescription];
    }];
    [self _updatePages:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}


#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _pages.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    DDPimaticGroup *group = _pages[indexPath.row];
    
    cell.textLabel.text   = group.name;
    
    return cell;
}

#pragma mark - Internal

- (void) _updatePages:(DDPimaticListChangedDescription*)description
{
    if ( description == nil )
    {
        _pages = [DDPimaticService sharedInstance].pages;
        [self.tableView reloadData];
    }
    else
    {
        _pages = description.list;
        switch (description.changedType) {
            case DDPimaticListChangedTypeInsertion:
                [self.tableView insertRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeRemoval:
                [self.tableView deleteRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeReplacement:
                [self.tableView reloadRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeReset:
            default:
                [self.tableView reloadData];
                break;
        }
    }
}

@end
