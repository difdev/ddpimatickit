//
//  VCDevices.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 17.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit.h>
#import "VCDevices.h"
#import "VCDeviceDetails.h"

@interface VCDevices ()<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *_devices;
    id _observerDevices;
}
@end

@implementation VCDevices

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Devices";
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _observerDevices = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceDeviceListChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self _updateDevices];
    }];
    [self _updateDevices];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:_observerDevices];
}


#pragma mark - TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _devices.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    DDPimaticDevice *device = _devices[indexPath.row];
    
    cell.textLabel.text = device.name;
    cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableView delegate selection

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    VCDeviceDetails *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VCDeviceDetails"];
    vc.device = _devices[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Internal

- (void) _updateDevices
{
    _devices = [DDPimaticService sharedInstance].devices;
    [self.tableView reloadData];
}

@end
