//
//  VCLogin.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 05.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//


#import <DDPimaticKit/DDPimaticKit.h>
#import "Constants.h"
#import "VCLogin.h"

@interface VCLogin ()

@property (weak, nonatomic) IBOutlet UITextField *textfieldURL;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;

@end

@implementation VCLogin

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Login";
    
    self.textfieldURL.text      = kDefaultServerUrl;
    self.textFieldUsername.text = kDefaultUsername;
    self.textFieldPassword.text = kDefaultPassword;
}

- (IBAction)actionLogin:(id)sender
{
    DDPimaticService *service = [DDPimaticService sharedInstance];
    
    service.serverURL = [NSURL URLWithString:self.textfieldURL.text];

    [service loginWithUsername:self.textFieldUsername.text password:self.textFieldPassword.text completion:^(id result, NSError *error) {
        if  (error != nil ) {
            NSLog(@"Login failed %@", error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Login failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];

}

@end
