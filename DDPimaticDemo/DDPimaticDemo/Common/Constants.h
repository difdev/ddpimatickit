//
//  Constants.h
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 02.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString* const kDefaultServerUrl;
extern NSString* const kDefaultUsername;
extern NSString* const kDefaultPassword;
