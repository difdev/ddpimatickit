//
//  AppDelegate.h
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 09.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

