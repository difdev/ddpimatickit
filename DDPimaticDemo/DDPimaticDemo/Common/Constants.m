//
//  Constants.m
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 02.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "Constants.h"

//NSString* const kDefaultServerUrl  = @"http://localhost:8080";
NSString* const kDefaultServerUrl  = @"http://demo.pimatic.org";
NSString* const kDefaultUsername   = @"demo";
NSString* const kDefaultPassword   = @"demo";
