//
//  CellDeviceActions.h
//  DDPimaticDemo
//
//  Created by Dirk Fischbach on 02.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellDeviceAction : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelDescription;
@property (nonatomic, weak) IBOutlet UILabel *labelParams;
@property (nonatomic, weak) IBOutlet UILabel *labelReturns;

@end
