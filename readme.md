#DDPimaticKit

##Overview
DDPimaticKit is an iOS framework which allows to access the [pimatic homeautomatic server](http://pimatic.org) from a native iOS environment.  
pimatic offers a rich [API](http://pimatic.org/guide/api/) which can be accessed by using the REST-API together with HTTP-Requests or by using the websocket-API together with websockts and socket.io. DDPimaticKit uses websockets, because it offers live updates for changes. [Socket.IO-Client-Swift](https://github.com/nuclearace/Socket.IO-Client-Swift) is used as socket.io client.  

The current implementation supports:

* readonly access to Devices, Variables, Groups, Rules
* live updates of this objects
* execution of actions

If you need support for other features of pimatic please contact me and I`ll extend the framework.  
Use the included demo app to see how to access the different objects.

##Quick start

* Copy the framework to your project directory and add it to your project.
* Include the header  ```#import <DDPimaticKit/DDPimaticKit.h>```
* Configure the server URL and login.

```
DDPimaticService *service = [DDPimaticService sharedInstance];
service.serverURL = [NSURL URLWithString:@"http://yourserver:port"];
[service loginWithUsername:@"yourusername" password:@"yourpassword" completion:^(id result, NSError *error) {
   if  (error != nil ) {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Login failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
   }
   else
   {
   		...
   		// setup your UI and display data
   		...
   }
}];

```

* In a UIViewController connect to the appropriate change notification for devices, variables, ... .

```
@interface AViewController ()
{
    id observerDevices;
}
@end

...

@implementation

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    observerDevices = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceDeviceListChangedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self updateDevices];
    }];
    [self updateDevices];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:observerDevices];
}


- (void) updateDevices
{
    NSArray  *devices = [DDPimaticService sharedInstance].devices;
    
    // use the data e.g. as a UITableView datasource.
    ....
}

@end

```

* Use the ```DDPimaticListChangedDescription``` which is attached to the changed notification for smooth UI updates of a table view for example:

```
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    observer = [[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceDeviceListChangedNotification object:nil queue:nil usingBlock:^(NSNotification *notification) {
        [self updateDevices:notification.pimaticServiceChangedListDescription];
    }];
    [self updateDevices:nil];
}

...

- (void) updateDevices:(DDPimaticListChangedDescription*)description
{
    if ( description == nil )
    {
    	// update all
        self.devices = [DDPimaticService sharedInstance].devices;
        [self.tableView reloadData];
    }
    else
    {
    	// try to update single rows
        self.devices = description.list;
        switch (description.changedType) {
            case DDPimaticListChangedTypeInsertion:
                [self.tableView insertRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeRemoval:
                [self.tableView deleteRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeReplacement:
                [self.tableView reloadRowsAtIndexPaths:[description.indexSet indexPathArrayWithSection:0] withRowAnimation:UITableViewRowAnimationFade];
                break;
            case DDPimaticListChangedTypeReset:
            default:
		    	// update all
                [self.tableView reloadData];
                break;
        }
    }
}
```

* Use the ```DDPimaticServiceDeviceChangedNotification``` to detect changes of a devices or its attributes.

```
[[NSNotificationCenter defaultCenter] addObserverForName:DDPimaticServiceDeviceChangedNotification  object:nil queue:nil usingBlock:^(NSNotification *notification) {
   DDPimaticDevice *device = notification.userInfo[DDPimaticObjectChangedKey];
   DDPimaticDeviceAttribute *attribute = notification.userInfo[DDPimaticDeviceAttributeChangedKey];
   ...
}];
```


##Detailed informations
For more a more detailed description see my [post](http://blog.difdev.de/ddpimatickit-accessing-pimatic-from-ios/). 



##License
MIT