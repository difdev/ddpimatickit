//
//  DDPimaticCategories.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 27.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>


@class DDPimaticDevice;
@class DDPimaticDeviceAction;
@class DDPimaticDeviceAttribute;
@class DDPimaticPage;
@class DDPimaticGroup;
@class DDPimaticRule;
@class DDPimaticVariable;

/**
 Category to access the defined notification parameters.
 */
@interface NSNotification(DDPimatic)

// The changed object.
@property (nonatomic, strong, readonly) id pimaticServiceChangedObject;

// The changed list description.
@property (nonatomic, strong, readonly) id pimaticServiceChangedListDescription;

@end

/**
 Category to get objects from an array with its ids.
 */
@interface NSArray(DDPimatic)

- (NSUInteger) indexOfPimaticVariableWithName:(NSString*)name;
- (DDPimaticVariable*) pimaticVariableWithName:(NSString*)name;

- (NSUInteger) indexOfPimaticDeviceWithUID:(NSString*)uid;
- (DDPimaticDevice*) pimaticDeviceWithUID:(NSString*)uid;

- (NSUInteger) indexOfPimaticDeviceAttributeWithName:(NSString*)name;
- (DDPimaticDeviceAttribute*) deviceAttributeWithName:(NSString*)name;

- (NSUInteger) indexOfPimaticDeviceActionWithName:(NSString*)name;
- (DDPimaticDeviceAction*) deviceActionWithName:(NSString*)name;

- (NSUInteger) indexOfPimaticGroupWithUID:(NSString*)uid;
- (DDPimaticGroup*) pimaticGroupWithUID:(NSString*)uid;

- (NSUInteger) indexOfPimaticPageWithUID:(NSString*)uid;
- (DDPimaticPage*) pimaticPageWithUID:(NSString*)uid;

- (NSUInteger) indexOfPimaticRuleWithUID:(NSString*)uid;
- (DDPimaticRule*) pimaticRuleWithUID:(NSString*)uid;

@end


@interface NSString(DDPimatic)

- (NSString*)pimaticStringByEscapingForQueryString;

@end


@interface NSDictionary(DDPimatic)

- (NSString*) pimaticQueryString;

@end


@interface NSDate(DDPimatic)

+ (instancetype) dateWithPimaticDate:(NSNumber*)number;

@end


@interface NSIndexSet(DDPimatic)

- (NSArray*) indexPathArrayWithSection:(NSUInteger)section;

@end