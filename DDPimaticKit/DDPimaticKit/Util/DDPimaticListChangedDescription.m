//
//  DDListChangedDescription.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticListChangedDescription.h"

@implementation DDPimaticListChangedDescription

+ (instancetype) listChangedDescriptionWithList:(NSArray*)list
{
    return [[DDPimaticListChangedDescription alloc] initWithList:list
                                              changedType:DDPimaticListChangedTypeReset
                                                 itemsOld:nil
                                                 itemsNew:nil
                                                 indexSet:nil];
}

+ (instancetype) listChangedDescriptionWithList:(NSArray*)list insertItem:(id)insertItem at:(NSUInteger)index
{
    return [[DDPimaticListChangedDescription alloc] initWithList:list
                                              changedType:DDPimaticListChangedTypeInsertion
                                                 itemsOld:nil
                                                 itemsNew:nil
                                                 indexSet:[NSIndexSet indexSetWithIndex:index]];
}

+ (instancetype) listChangedDescriptionWithList:(NSArray*)list removeItem:(id)removeItem at:(NSUInteger)index
{
    return [[DDPimaticListChangedDescription alloc] initWithList:list
                                              changedType:DDPimaticListChangedTypeRemoval
                                                 itemsOld:nil
                                                 itemsNew:nil
                                                 indexSet:[NSIndexSet indexSetWithIndex:index]];
    
}

+ (instancetype) listChangedDescriptionWithList:(NSArray*)list replaceItem:(id)replaceItem at:(NSUInteger)index
{
    return [[DDPimaticListChangedDescription alloc] initWithList:list
                                              changedType:DDPimaticListChangedTypeReplacement
                                                 itemsOld:nil
                                                 itemsNew:nil
                                                 indexSet:[NSIndexSet indexSetWithIndex:index]];
    
}


- (instancetype) initWithList:(NSArray*)list changedType:(DDPimaticListChangedType)changedType itemsOld:(NSArray*)itemsOld itemsNew:(NSArray*)itemsNew indexSet:(NSIndexSet*)indexSet
{
    self = [super init];
    if (self) {
        self.list           = list;
        self.changedType    = changedType;
        self.itemsOld       = itemsOld;
        self.itemsNew       = itemsNew;
        self.indexSet       = indexSet;
    }
    return self;
}
@end
