//
//  DDPimaticCategories.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 27.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DDPimaticCategories.h"
#import "DDPimaticDevice.h"
#import "DDPimaticDeviceAction.h"
#import "DDPimaticDeviceAttribute.h"
#import "DDPimaticGroup.h"
#import "DDPimaticPage.h"
#import "DDPimaticRule.h"
#import "DDPimaticService.h"
#import "DDPimaticVariable.h"


@implementation NSNotification(DDPimatic)

- (id) pimaticServiceChangedObject
{
    return self.userInfo[DDPimaticObjectChangedKey];
}

- (id) pimaticServiceChangedListDescription
{
    return self.userInfo[DDPimaticListChangedDescriptionKey];
}

@end


@implementation NSArray(DDPimatic)

- (NSUInteger) indexOfPimaticVariableWithName:(NSString*)name
{
    for (NSUInteger i=0; i<self.count; i++) {
        DDPimaticVariable *variable = self[i];
        if ( [variable.name isEqualToString:name] ) {
            return i;
        }
    }
    return NSNotFound;
}

- (DDPimaticVariable*) pimaticVariableWithName:(NSString*)name
{
    for (DDPimaticVariable *variable in self) {
        if ( [variable.name isEqualToString:name] ) {
            return variable;
        }
    }
    return nil;
}


- (NSUInteger) indexOfPimaticDeviceWithUID:(NSString*)uid
{
    for (NSUInteger i=0; i<self.count; i++) {
        DDPimaticDevice *device = self[i];
        if ( [device.uid isEqualToString:uid] ) {
            return i;
        }
    }
    return NSNotFound;
}

- (DDPimaticDevice*) pimaticDeviceWithUID:(NSString*)uid
{
    for (DDPimaticDevice *device in self) {
        if ( [device.uid isEqualToString:uid] ) {
            return device;
        }
    }
    return nil;
}

- (NSUInteger) indexOfPimaticDeviceAttributeWithName:(NSString*)name
{
    for (NSUInteger i=0; i<self.count; i++) {
        DDPimaticDeviceAttribute *attribute = self[i];
        if ( [attribute.name isEqualToString:name] ) {
            return i;
        }
    }
    return NSNotFound;
}

- (DDPimaticDeviceAttribute*) deviceAttributeWithName:(NSString*)name
{
    for (DDPimaticDeviceAttribute *attribute in self) {
        if ( [attribute.name isEqualToString:name] ) {
            return attribute;
        }
    }
    return nil;
}

- (NSUInteger) indexOfPimaticDeviceActionWithName:(NSString*)name
{
    for (NSUInteger i=0; i<self.count; i++) {
        DDPimaticDeviceAction *action = self[i];
        if ( [action.name isEqualToString:name] ) {
            return i;
        }
    }
    return NSNotFound;
}

- (DDPimaticDeviceAction*) deviceActionWithName:(NSString*)name
{
    for (DDPimaticDeviceAction *action in self) {
        if ( [action.name isEqualToString:name] ) {
            return action;
        }
    }
    return nil;    
}

- (NSUInteger) indexOfPimaticGroupWithUID:(NSString*)uid
{
    for (NSUInteger i=0; i<self.count; i++) {
        DDPimaticGroup *group = self[i];
        if ( [group.uid isEqualToString:uid] ) {
            return i;
        }
    }
    return NSNotFound;
}

- (DDPimaticGroup*) pimaticGroupWithUID:(NSString*)uid
{
    for (DDPimaticGroup *group in self) {
        if ( [group.uid isEqualToString:uid] ) {
            return group;
        }
    }
    return nil;
}

- (NSUInteger) indexOfPimaticPageWithUID:(NSString*)uid
{
    for (NSUInteger i=0; i<self.count; i++) {
        DDPimaticPage *page = self[i];
        if ( [page.uid isEqualToString:uid] ) {
            return i;
        }
    }
    return NSNotFound;
}

- (DDPimaticPage*) pimaticPageWithUID:(NSString*)uid
{
    for (DDPimaticPage *page in self) {
        if ( [page.uid isEqualToString:uid] ) {
            return page;
        }
    }
    return nil;
}

- (NSUInteger) indexOfPimaticRuleWithUID:(NSString*)uid
{
    for (NSUInteger i=0; i<self.count; i++) {
        DDPimaticRule *rule = self[i];
        if ( [rule.uid isEqualToString:uid] ) {
            return i;
        }
    }
    return NSNotFound;
}

- (DDPimaticRule*) pimaticRuleWithUID:(NSString*)uid
{
    for (DDPimaticRule *rule in self) {
        if ( [rule.uid isEqualToString:uid] ) {
            return rule;
        }
    }
    return nil;
}

@end


@implementation NSString(DDPimatic)

- (NSString*)pimaticStringByEscapingForQueryString
{
    if ( [self respondsToSelector:@selector(stringByAddingPercentEncodingWithAllowedCharacters:)] )
    {
        NSCharacterSet *charSet = [[NSCharacterSet characterSetWithCharactersInString:@"!*'\"();:@&=+$,/?%#[]% "] invertedSet];
        return [self stringByAddingPercentEncodingWithAllowedCharacters:charSet];
    }
    
    NSString    *result     = self;
    CFStringRef org         = (__bridge CFStringRef) self;
    CFStringRef ignore      = CFSTR(" ");
    CFStringRef toEscape    = CFSTR("\n\r?[]()$,!'*;:@&=#%+/");
    
    CFStringRef escaped = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, org, ignore, toEscape, kCFStringEncodingUTF8);
    if ( escaped != nil)
    {
        NSMutableString *mutable = [NSMutableString stringWithString:(__bridge NSString *)escaped];
        CFRelease(escaped);
        
        [mutable replaceOccurrencesOfString:@" " withString:@"+" options:0 range:NSMakeRange(0, [mutable length])];
        result = mutable;
    }
    return result;
}

@end


@implementation NSDictionary(DDPimatic)

- (NSString*) pimaticQueryString
{
    NSMutableArray* arguments = [NSMutableArray arrayWithCapacity:[self count]];
    for (NSString* key in self)
    {
        [arguments addObject:[NSString stringWithFormat:@"%@=%@",
                              [key pimaticStringByEscapingForQueryString],
                              [[[self objectForKey:key] description] pimaticStringByEscapingForQueryString]]];
    }
    return [arguments componentsJoinedByString:@"&"];
}

@end


@implementation NSDate(DDPimatic)

+ (instancetype) dateWithPimaticDate:(NSNumber*)number
{
    return [NSDate dateWithTimeIntervalSince1970:(number.doubleValue/1000.0)];
}

@end


@implementation NSIndexSet(DDPimatic)

- (NSArray*) indexPathArrayWithSection:(NSUInteger)section
{
    NSMutableArray *array = [NSMutableArray new];
    
    NSUInteger currentIndex = [self firstIndex];
    while (currentIndex != NSNotFound) {
        [array addObject:[NSIndexPath indexPathForRow:currentIndex inSection:section]];
        currentIndex = [self indexGreaterThanIndex:currentIndex];
    }

    return array;
}

@end