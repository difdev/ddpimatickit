//
//  DDListChangedDescription.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, DDPimaticListChangedType) {
    DDPimaticListChangedTypeReset          = 0,
    DDPimaticListChangedTypeInsertion      = 1,
    DDPimaticListChangedTypeRemoval        = 2,
    DDPimaticListChangedTypeReplacement    = 3
};


/**
 A description about the changes in a list to optimize the UI update.
 */
@interface DDPimaticListChangedDescription : NSObject

+ (instancetype) listChangedDescriptionWithList:(NSArray*)list;
+ (instancetype) listChangedDescriptionWithList:(NSArray*)list insertItem:(id)insertItem at:(NSUInteger)index;
+ (instancetype) listChangedDescriptionWithList:(NSArray*)list removeItem:(id)removeItem at:(NSUInteger)index;
+ (instancetype) listChangedDescriptionWithList:(NSArray*)list replaceItem:(id)replaceItem at:(NSUInteger)index;

- (instancetype) initWithList:(NSArray*)list changedType:(DDPimaticListChangedType)changedType itemsOld:(NSArray*)itemsOld itemsNew:(NSArray*)itemsNew indexSet:(NSIndexSet*)indexSet;

@property (nonatomic, assign) DDPimaticListChangedType changedType;

@property (nonatomic, strong) NSArray       *list;
@property (nonatomic, strong) NSArray       *itemsOld;
@property (nonatomic, strong) NSArray       *itemsNew;
@property (nonatomic, strong) NSIndexSet    *indexSet;

@end
