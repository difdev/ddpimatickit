//
//  DDPimaticKit.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 09.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DDPimaticKit.
FOUNDATION_EXPORT double DDPimaticKitVersionNumber;

//! Project version string for DDPimaticKit.
FOUNDATION_EXPORT const unsigned char DDPimaticKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DDPimaticKit/PublicHeader.h>


// Service
#import <DDPimaticKit/DDPimaticService.h>

// Model
#import <DDPimaticKit/DDPimaticDevice.h>
#import <DDPimaticKit/DDPimaticDeviceAction.h>
#import <DDPimaticKit/DDPimaticDeviceAttribute.h>
#import <DDPimaticKit/DDPimaticDeviceAttributeChanged.h>
#import <DDPimaticKit/DDPimaticGroup.h>
#import <DDPimaticKit/DDPimaticHistoryItem.h>
#import <DDPimaticKit/DDPimaticRule.h>
#import <DDPimaticKit/DDPimaticUserInfo.h>
#import <DDPimaticKit/DDPimaticUserPermissions.h>
#import <DDPimaticKit/DDPimaticVariable.h>

// Util
#import <DDPimaticKit/DDPimaticCategories.h>
#import <DDPimaticKit/DDPimaticListChangedDescription.h>

//#import <DDPimaticKit/DDPimaticKit-Swift.h>

