//
//  DDPimaticService.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <DDPimaticKit/DDPimaticKit-Swift.h>
#import "DDPimaticBaseObject.h"
#import "DDPimaticCategories.h"
#import "DDPimaticDevice.h"
#import "DDPimaticDeviceAction.h"
#import "DDPimaticDeviceAttribute.h"
#import "DDPimaticDeviceAttributeChanged.h"
#import "DDPimaticGroup.h"
#import "DDPimaticHistoryItem.h"
#import "DDPimaticListChangedDescription.h"
#import "DDPimaticRule.h"
#import "DDPimaticService.h"
#import "DDPimaticVariable.h"

// error domain
NSString* const DDPimaticServiceErrorDomain = @"DDPimaticServiceErrorDomain";

// notifications
NSString* const DDPimaticServiceLoginStateChangedNotification       = @"DDPimaticServiceLoginStateChangedNotification";
NSString* const DDPimaticServiceUserInfoChangedNotification         = @"DDPimaticServiceUserInfoChangedNotification";
NSString* const DDPimaticServiceDeviceListChangedNotification       = @"DDPimaticServiceDevicesChangedNotification";
NSString* const DDPimaticServiceDeviceChangedNotification           = @"DDPimaticServiceDeviceChangedNotification";
NSString* const DDPimaticServiceDeviceAttributeChangedNotification  = @"DDPimaticServiceDeviceAttributeChangedNotification";
NSString* const DDPimaticServiceVariableListChangedNotification     = @"DDPimaticServiceVariablesChangedNotification";
NSString* const DDPimaticServiceVariableChangedNotification         = @"DDPimaticServiceVariableChangedNotification";
NSString* const DDPimaticServiceGroupListChangedNotification        = @"DDPimaticServiceGroupsChangedNotification";
NSString* const DDPimaticServiceRuleListChangedNotification         = @"DDPimaticServiceRuleListChangedNotification";
NSString* const DDPimaticServicePageListChangedNotification         = @"DDPimaticServicePageListChangedNotification";

// Notification parameters
NSString* const DDPimaticObjectChangedKey                           = @"DDPimaticObjectChangedKey";
NSString* const DDPimaticListChangedDescriptionKey                  = @"DDPimaticListChangedDescriptionKey";
NSString* const DDPimaticDeviceAttributeChangedKey                  = @"DDPimaticDeviceAttributeChangedKey";


@interface DDPimaticService() <NSURLSessionDelegate>
{
    NSURLSession    *_urlSession;
    SocketIOClient  *_socketIOClient;
}

@property (nonatomic, strong) NSString *currentUsername;
@property (nonatomic, strong) NSString *currentPassword;
@property (nonatomic, strong) DDPimaticUserInfo *userInfo;
@property (nonatomic, strong) NSMutableArray *devicesIntern;
@property (nonatomic, strong) NSMutableArray *variablesIntern;
@property (nonatomic, strong) NSMutableArray *groupsIntern;
@property (nonatomic, strong) NSMutableArray *rulesIntern;
@property (nonatomic, strong) NSMutableArray *pagesIntern;

@end


@implementation DDPimaticService


+ (instancetype) sharedInstance
{
    static DDPimaticService *_sDDPimaticService = nil;
    static dispatch_once_t  _sDDPimaticServiceOnceToken;
    
    dispatch_once(&_sDDPimaticServiceOnceToken, ^{
        _sDDPimaticService = [[self alloc] init];
    });
    return _sDDPimaticService;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self _internalInit];
    }
    return self;
}


#pragma mark - Properties

- (BOOL) isLoggedin
{
    return self.currentUsername != nil;
}

- (void) setUserInfo:(DDPimaticUserInfo *)userInfo
{
    _userInfo = userInfo;
    [self _postObjectChangedNotificationWithName:DDPimaticServiceUserInfoChangedNotification changedObject:_userInfo];
}

- (void) setDevicesIntern:(NSMutableArray *)devicesIntern
{
    _devicesIntern = devicesIntern;
    [self _postListChangedNotificationWithName:DDPimaticServiceDeviceListChangedNotification
                        listChangedDescription:[DDPimaticListChangedDescription listChangedDescriptionWithList:_devicesIntern]];
}

- (NSArray*) devices
{
    return [NSArray arrayWithArray:_devicesIntern];
}

- (void) setVariablesIntern:(NSMutableArray *)variablesIntern
{
    _variablesIntern = variablesIntern;
    [self _postListChangedNotificationWithName:DDPimaticServiceVariableListChangedNotification
                        listChangedDescription:[DDPimaticListChangedDescription listChangedDescriptionWithList:_variablesIntern]];
}

- (NSArray*) variables
{
    return [NSArray arrayWithArray:_variablesIntern];
}

- (void) setGroupsIntern:(NSMutableArray *)groupsIntern
{
    _groupsIntern = groupsIntern;
    [self _postListChangedNotificationWithName:DDPimaticServiceGroupListChangedNotification
                        listChangedDescription:[DDPimaticListChangedDescription listChangedDescriptionWithList:_groupsIntern]];
}

- (NSArray*) groups
{
    return [NSArray arrayWithArray:_groupsIntern];
}

- (void) setRulesIntern:(NSMutableArray *)rulesIntern
{
    _rulesIntern = rulesIntern;
    [self _postListChangedNotificationWithName:DDPimaticServiceRuleListChangedNotification
                        listChangedDescription:[DDPimaticListChangedDescription listChangedDescriptionWithList:_rulesIntern]];
}

- (NSArray*) rules
{
    return [NSArray arrayWithArray:_rulesIntern];
}

- (void) setPagesIntern:(NSMutableArray *)pagesIntern
{
    _pagesIntern = pagesIntern;
    [self _postListChangedNotificationWithName:DDPimaticServicePageListChangedNotification
                        listChangedDescription:[DDPimaticListChangedDescription listChangedDescriptionWithList:_pagesIntern]];
}

- (NSArray*) pages
{
    return [NSArray arrayWithArray:_pagesIntern];
}


#pragma mark - Methods

- (void) loginWithUsername:(NSString*)username password:(NSString*)password completion:(DDPimaticCompletionHandler)completion
{
    self.currentUsername = username;
    self.currentPassword = password;
    
    NSString *params = [NSString stringWithFormat:@"username=%@&password=%@", username, password];
    NSMutableURLRequest *urlRequest = [self _createPostRequestWithPath:@"login" paramString:params];
    
    NSURLSessionDataTask *dataTask =[_urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        // handle on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *finalError = error;
            if ( finalError == nil ) {
                finalError = [self _getErrorFromResponse:response error:error];
            }
            NSLog(@"Response:%@ %@\n", response, finalError);
            
            if (finalError == nil)
            {
                [self _initSocketIOWithCookies:[NSHTTPCookie cookiesWithResponseHeaderFields:((NSHTTPURLResponse*)response).allHeaderFields forURL:urlRequest.URL]];
            }
            else
            {
                self.currentUsername = nil;
                self.currentPassword = nil;
            }
            if ( completion ) {
                completion(nil, finalError);
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:DDPimaticServiceLoginStateChangedNotification object:self];
        });
    }];
    [dataTask resume];
}

- (void) logoutWithCompletion:(DDPimaticCompletionHandler)completion
{
    self.currentUsername = nil;
    self.currentPassword = nil;
    
    [self _resetCachedData];
    
    [_socketIOClient disconnectWithFast:YES];
    _socketIOClient = nil;
    
    NSMutableURLRequest *urlRequest = [self _createGetRequestWithPath:@"logout" paramString:nil];
    NSURLSessionDataTask *dataTask  = [_urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        // handle on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *finalError = error;
            if ( finalError == nil ) {
                finalError = [self _getErrorFromResponse:response error:error];
            }
            NSLog(@"Response:%@ %@\n", response, finalError);
            if ( completion != nil ) {
                completion(nil, finalError);
            }
            return;
        });
    }];
    [dataTask resume];
}

- (void) executeDeviceAction:(DDPimaticDeviceAction*)action deviceUID:(NSString*)uid params:(NSDictionary*)params completion:(DDPimaticCompletionHandler)completion
{
    NSMutableString *path = [NSMutableString stringWithFormat:@"api/device/%@/%@", uid, action.name];
    
    // append param as query string?
    if ( params != nil ) {
        [path appendFormat:@"?%@", [self _convertTypesWithActionParams:action.params callParams:params].pimaticQueryString];
    }
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path relativeToURL:self.serverURL]];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask =[_urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
         dispatch_async(dispatch_get_main_queue(), ^{
             NSError *finalError = error;
             if ( finalError == nil ) {
                 finalError = [self _getErrorFromResponse:response error:error];
             }
             NSError *errorSerialize;
             id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&errorSerialize];
             if ( finalError == nil && errorSerialize != nil )
             {
                 NSLog(@"NSJSONSerialization error %@", errorSerialize);
                 finalError = errorSerialize;
             }
             NSError *errorJson = [self _getErrorFromJson:json];
             if ( errorJson != nil ) {
                 finalError = errorJson;
             }
             
             if ( completion )
             {
                 id result = nil;
                 if ( error == nil )
                 {
                     if ( action.returns.count > 0 )
                     {
                         // return the results of a get
                         result = json[@"result"];
                     }
                     else
                     {
                         // return the results of a set
                         NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:json];
                         [dict removeObjectForKey:@"success"];
                         result = dict;
                     }
                 }
                 completion(result, finalError);
             }
             
         });
    }];
    [dataTask resume];
}


#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    NSLog(@"URLSession didBecomeInvalidWithError %@", error);
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    NSLog(@"URLSession didReceiveChallenge %@", challenge);
}


#pragma mark - Internal

- (void) _internalInit
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    _urlSession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
}

- (void) _resetCachedData
{
    self.userInfo   = nil;
}


- (void) _postObjectChangedNotificationWithName:(NSString*)name changedObject:(id)changedObject
{
    NSDictionary *userInfo = (changedObject != nil) ? @{DDPimaticObjectChangedKey:changedObject} : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:userInfo];
}

- (void) _postListChangedNotificationWithName:(NSString*)name listChangedDescription:(DDPimaticListChangedDescription*)description
{
    NSDictionary *userInfo = (description != nil) ? @{DDPimaticListChangedDescriptionKey:description} : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:userInfo];
}

- (NSError*) _getErrorFromJson:(NSDictionary*)json
{
    BOOL success = ((NSNumber*)json[@"success"]).boolValue;
    if ( !success ) {
        NSString *message = json[@"message"];
        return [NSError errorWithDomain:DDPimaticServiceErrorDomain code:DDPimaticServiceError_Server userInfo:@{NSLocalizedDescriptionKey:message}];
    }
    return nil;
}

- (NSDictionary*) _convertTypesWithActionParams:(NSDictionary*)actionParams callParams:(NSDictionary*)callParams
{
    NSMutableDictionary *converted = [NSMutableDictionary dictionaryWithDictionary:callParams];
    for (id key in actionParams) {
        NSDictionary *info = actionParams[key];
        NSString *type = info[@"type"];
        if ( [type isEqualToString:@"boolean"] ) {
            converted[key] = ((NSNumber*)converted[key]).boolValue ? @"true" : @"false";
        }
    }
    return converted;
}

#pragma mark - Network

- (NSError*) _getErrorFromResponse:(NSURLResponse*)response error:(NSError*)error
{
    if ( error != nil )
    {
        NSDictionary *userInfo = @{@"internalError":error};
        return [NSError errorWithDomain:DDPimaticServiceErrorDomain code:DDPimaticServiceError_Network userInfo:userInfo];
    }

    NSInteger statusCode = 200;
    
    if ( [response isKindOfClass:[NSHTTPURLResponse class]] ) {
        statusCode = ((NSHTTPURLResponse *)response).statusCode;
    }

    if ( statusCode == 401 ) {
        return [NSError errorWithDomain:DDPimaticServiceErrorDomain code:DDPimaticServiceError_Authentication userInfo:nil];
    } else if ( statusCode >= 400 ) {
        return [NSError errorWithDomain:DDPimaticServiceErrorDomain code:DDPimaticServiceError_Server userInfo:nil];
    }

    return nil;
}

- (NSMutableURLRequest*) _createGetRequestWithPath:(NSString*)path paramString:(NSString*)paramString
{
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path relativeToURL:self.serverURL]];
    [urlRequest setHTTPMethod:@"GET"];
    
    return urlRequest;
}

- (NSMutableURLRequest*) _createPostRequestWithPath:(NSString*)path paramString:(NSString*)paramString
{
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path relativeToURL:self.serverURL]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    return urlRequest;
}

- (void) _getWithPath:(NSString*)path keyName:(NSString*)keyName className:(NSString*)className completion:(DDPimaticCompletionHandler)completion
{
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path relativeToURL:self.serverURL]];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask =[_urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *finalError = error;
            if ( finalError == nil ) {
                 finalError = [self _getErrorFromResponse:response error:error];
            }

            if ( completion )
            {
                id result = nil;
                if (  error == nil )
                {
                    NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                    NSLog(@"_getWithPath %@: %@",path, text);
                    
                    NSError *errorJSON;
                    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&errorJSON];
                    if ( errorJSON != nil )
                    {
                        NSLog(@"NSJSONSerialization error %@", errorJSON);
                        finalError = errorJSON;
                    }
                    else
                    {
                        // get the requested data
                        json = json[keyName];
                        
                        Class objectClass = NSClassFromString(className);
                        if ( [json isKindOfClass:[NSArray class]] )
                        {
                            NSMutableArray *array = [NSMutableArray new];
                            // array of objects
                            for (id jsonObject in json)
                            {
                                id entry = [[objectClass alloc] initWithJSON:jsonObject];
                                [array addObject:entry];
                            }
                            result = array;
                        }
                        else
                        {
                            // single object
                            result = [[objectClass alloc] initWithJSON:json];
                        }
                    }
                }
                completion(result, finalError);
            }
        });
    }];
    [dataTask resume];
}




#pragma mark - handle Socket.io

- (void) _initSocketIOWithCookies:(NSArray*)cookies
{
    NSString *socketUrl = self.serverURL.absoluteString;
    _socketIOClient = [[SocketIOClient alloc] initWithSocketURL:socketUrl options:nil];
    _socketIOClient.cookies = cookies;
    
    // connect callbacks

    //----------------------------------------------
    // common
    //----------------------------------------------
    [_socketIOClient onAny:^(SocketAnyEvent *anyEvent) {
        NSLog(@"onAny %@ %@", anyEvent.event, anyEvent.items);
    }];
    
    [_socketIOClient on: @"connect" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
//        NSLog(@"connected");
        //        [socket emitObjc:@"echo" withItems:@[@"echo test"]];
        //        [socket emitWithAckObjc:@"ackack" withItems:@[@1]](10, ^(NSArray* data) {
        //            NSLog(@"Got ack");
        //        });
    }];

    
    [_socketIOClient on: @"hello" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        if ( data.count > 0 ) {
            self.userInfo = [[DDPimaticUserInfo alloc] initWithJSON:data[0]];
        } else {
            self.userInfo = nil;
        }
    }];

    
    //----------------------------------------------
    // devices
    //----------------------------------------------
    [_socketIOClient on: @"devices" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        self.devicesIntern = [self _socketioGetObjectsWithClassName:@"DDPimaticDevice" dataArray:data];
    }];
    
    

    [_socketIOClient on: @"deviceAttributeChanged" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        DDPimaticDeviceAttributeChanged *changed = [self _socketioGetObjectWithClassName:@"DDPimaticDeviceAttributeChanged" dataArray:data];
        
        // adjust the device
        DDPimaticDevice             *device     = nil;
        DDPimaticDeviceAttribute    *attribute  = nil;
        NSUInteger index = [self.devices indexOfPimaticDeviceWithUID:changed.deviceId];
        if ( index != NSNotFound )
        {
            device = self.devices[index];
            index = [device.attributes indexOfPimaticDeviceAttributeWithName:changed.attributeName];
            if ( index != NSNotFound )
            {
                attribute = device.attributes[index];
                attribute.lastUpdate    = changed.time;
                attribute.value         = changed.value;
                [attribute.history addObject:[DDPimaticHistoryItem historyItemWithDate:changed.time value:changed.value]];
            }
        }
        
        [self _postObjectChangedNotificationWithName:DDPimaticServiceDeviceAttributeChangedNotification changedObject:changed];
        if ( attribute != nil )
        {
            NSDictionary *userInfo = @{DDPimaticObjectChangedKey:device, DDPimaticDeviceAttributeChangedKey:attribute} ;
            [[NSNotificationCenter defaultCenter] postNotificationName:DDPimaticServiceDeviceChangedNotification object:self userInfo:userInfo];
        }
        
//        NSLog(@"deviceAttributeChanged %@", changed);
    }];

    
    //----------------------------------------------
    // variables
    //----------------------------------------------
    [_socketIOClient on: @"variables" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        self.variablesIntern = [self _socketioGetObjectsWithClassName:@"DDPimaticVariable" dataArray:data];
    }];

    [_socketIOClient on: @"variableValueChanged" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        if ( data.count > 0 )
        {
            NSDictionary *json = data[0];
            NSString *name = json[@"variableName"];
            if ( name.length > 0 )
            {
                NSUInteger index = [self.variables indexOfPimaticVariableWithName:name];
                if ( index != NSNotFound )
                {
                    DDPimaticVariable *variable = self.variables[index];
                    variable.value = json[@"variableValue"];
                    [self _postObjectChangedNotificationWithName:DDPimaticServiceVariableChangedNotification changedObject:variable];
                    
//                    NSLog(@"variable changed %@", variable);
                }
            }
        }
    }];

    
    //----------------------------------------------
    // groups
    //----------------------------------------------
    [_socketIOClient on: @"groups" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        self.groupsIntern = [self _socketioGetObjectsWithClassName:@"DDPimaticGroup" dataArray:data];
    }];
    
    [_socketIOClient on: @"groupAdded" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id group = [self _socketioGetObjectWithClassName:@"DDPimaticGroup" dataArray:data];
        [_groupsIntern addObject:group];
        
        DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.groups insertItem:group at:_groupsIntern.count-1];
        [self _postListChangedNotificationWithName:DDPimaticServiceGroupListChangedNotification listChangedDescription:changed];
    }];

    [_socketIOClient on: @"groupRemoved" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id group = [self _socketioGetObjectWithClassName:@"DDPimaticGroup" dataArray:data];
        NSUInteger index = [_groupsIntern indexOfObject:group];
        if ( index != NSNotFound ) {
            [_groupsIntern removeObjectAtIndex:index];
            DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.groups removeItem:group at:index];
            [self _postListChangedNotificationWithName:DDPimaticServiceGroupListChangedNotification listChangedDescription:changed];
        }
    }];

    [_socketIOClient on: @"groupChanged" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id group = [self _socketioGetObjectWithClassName:@"DDPimaticGroup" dataArray:data];
        NSUInteger index = [_groupsIntern indexOfObject:group];
        if ( index != NSNotFound ) {
            [_groupsIntern replaceObjectAtIndex:index withObject:group];
            DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.groups replaceItem:group at:index];
            [self _postListChangedNotificationWithName:DDPimaticServiceGroupListChangedNotification listChangedDescription:changed];
        }
    }];
    
    
    //----------------------------------------------
    // rules
    //----------------------------------------------
    [_socketIOClient on: @"rules" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        self.rulesIntern = [self _socketioGetObjectsWithClassName:@"DDPimaticRule" dataArray:data];
    }];

    [_socketIOClient on: @"ruleAdded" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id rule = [self _socketioGetObjectWithClassName:@"DDPimaticRule" dataArray:data];
        [_rulesIntern addObject:rule];
        
        DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.rules insertItem:rule at:_rulesIntern.count-1];
        [self _postListChangedNotificationWithName:DDPimaticServiceRuleListChangedNotification listChangedDescription:changed];
    }];
    
    [_socketIOClient on: @"ruleRemoved" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id rule = [self _socketioGetObjectWithClassName:@"DDPimaticRule" dataArray:data];
        NSUInteger index = [_rulesIntern indexOfObject:rule];
        if ( index != NSNotFound ) {
            [_rulesIntern removeObjectAtIndex:index];
            DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.rules removeItem:rule at:index];
            [self _postListChangedNotificationWithName:DDPimaticServiceRuleListChangedNotification listChangedDescription:changed];
        }
    }];
    
    [_socketIOClient on: @"ruleChanged" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id rule = [self _socketioGetObjectWithClassName:@"DDPimaticRule" dataArray:data];
        NSUInteger index = [_rulesIntern indexOfObject:rule];
        if ( index != NSNotFound ) {
            [_rulesIntern replaceObjectAtIndex:index withObject:rule];
            DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.rules replaceItem:rule at:index];
            [self _postListChangedNotificationWithName:DDPimaticServiceRuleListChangedNotification listChangedDescription:changed];
        }
    }];

    //----------------------------------------------
    // pages
    //----------------------------------------------
    [_socketIOClient on: @"pages" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        self.pagesIntern = [self _socketioGetObjectsWithClassName:@"DDPimaticPage" dataArray:data];
    }];

    [_socketIOClient on: @"pageAdded" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id page = [self _socketioGetObjectWithClassName:@"DDPimaticPage" dataArray:data];
        [_pagesIntern addObject:page];
        
        DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.pages insertItem:page at:_pagesIntern.count-1];
        [self _postListChangedNotificationWithName:DDPimaticServicePageListChangedNotification listChangedDescription:changed];
    }];
    
    [_socketIOClient on: @"pageRemoved" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id page = [self _socketioGetObjectWithClassName:@"DDPimaticPage" dataArray:data];
        NSUInteger index = [_pagesIntern indexOfObject:page];
        if ( index != NSNotFound ) {
            [_pagesIntern removeObjectAtIndex:index];
            DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.pages removeItem:page at:index];
            [self _postListChangedNotificationWithName:DDPimaticServicePageListChangedNotification listChangedDescription:changed];
        }
    }];
    
    [_socketIOClient on: @"pageChanged" callback: ^(NSArray* data, void (^ack)(NSArray*)) {
        id page = [self _socketioGetObjectWithClassName:@"DDPimaticPage" dataArray:data];
        NSUInteger index = [_pagesIntern indexOfObject:page];
        if ( index != NSNotFound ) {
            [_pagesIntern replaceObjectAtIndex:index withObject:page];
            DDPimaticListChangedDescription *changed = [DDPimaticListChangedDescription listChangedDescriptionWithList:self.pages replaceItem:page at:index];
            [self _postListChangedNotificationWithName:DDPimaticServicePageListChangedNotification listChangedDescription:changed];
        }
    }];


    // connect to the socket
    [_socketIOClient connect];
}

- (NSMutableArray*) _socketioGetObjectsWithClassName:(NSString*)className dataArray:(NSArray*)dataArray
{
    if ( dataArray.count < 1 ) {
        return nil;
    }
    
    Class objectClass = NSClassFromString(className);
    
    NSMutableArray *array = [NSMutableArray new];
    for (id jsonObject in dataArray[0])
    {
        id entry = [[objectClass alloc] initWithJSON:jsonObject];
        [array addObject:entry];
    }
    return array;
}

- (id) _socketioGetObjectWithClassName:(NSString*)className dataArray:(NSArray*)dataArray
{
    if ( dataArray.count < 1 ) {
        return nil;
    }
    
    Class objectClass = NSClassFromString(className);
    return [[objectClass alloc] initWithJSON:dataArray[0]];
}

@end

