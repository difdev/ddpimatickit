//
//  DDPimaticService.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticUserInfo.h"


/**
 Common completion handler for all async operations.
 @param result The result of the operation or nil on error.
 @param error  An error if operation failed or nil.
 */
typedef void (^DDPimaticCompletionHandler)(id result, NSError *error);

/**
 Notification fired on changes of the login state.
 */
extern NSString* const DDPimaticServiceLoginStateChangedNotification;

/**
 Notification fired on changes of the user info.
 */
extern NSString* const DDPimaticServiceUserInfoChangedNotification;

/**
 Notification fired on changes of the device list.
 */
extern NSString* const DDPimaticServiceDeviceListChangedNotification;

/**
 Notification fired on changes of a device.
 */
extern NSString* const DDPimaticServiceDeviceChangedNotification;

/**
 Notification fired on changes of the variable list.
 */
extern NSString* const DDPimaticServiceVariableListChangedNotification;

/**
 Notification fired on changes of the value of a single variable.
 */
extern NSString* const DDPimaticServiceVariableChangedNotification;

/**
 Notification fired on changes of the group list.
 */
extern NSString* const DDPimaticServiceGroupListChangedNotification;

/**
 Notification fired on changes of the rule list.
 */
extern NSString* const DDPimaticServiceRuleListChangedNotification;

/**
 Notification fired on changes of the page list.
 */
extern NSString* const DDPimaticServicePageListChangedNotification;

/**
 Notification fired on changes of the value of a single variable.
 */
extern NSString* const DDPimaticServiceDeviceAttributeChangedNotification;



/**
 The key for the notification userinfo dictionary to get the changed object.
 */
extern NSString* const DDPimaticObjectChangedKey;

/**
 The key for the notification userinfo dictionary to get the changed description for a list.
 */
extern NSString* const DDPimaticListChangedDescriptionKey;


/**
 The error domain of the DDPimaticService.
 */
extern NSString* const DDPimaticServiceErrorDomain;

/**
 The error codes of the DDPimaticServiceErrorDomain.
 */
typedef NS_ENUM(NSInteger, DDPimaticServiceError){
    /**
     No Error.
     */
    DDPimaticServiceError_OK                = 0,
    /**
     Network error.
     */
    DDPimaticServiceError_Network           = 10,
    /**
     Authentication error.
     */
    DDPimaticServiceError_Authentication    = 11,
    /**
     Generic error returned by the pimatic server.
     */
    DDPimaticServiceError_Server            = 12
};

@class DDPimaticDeviceAction;

/**
 DDPimaticService is the central service to use to connect with the pimatic home automation server.
 */
@interface DDPimaticService : NSObject

/**
 The singleton used to access the service.
 @return The shared instance.
 */
+ (instancetype) sharedInstance;

/**
 The URL of the pimatic server.
 */
@property (nonatomic, strong) NSURL *serverURL;

/**
 Returns YES if a user is logged in.
 */
@property (nonatomic, assign, readonly) BOOL isLoggedin;

/**
 The username of the current user.
 */
@property (nonatomic, strong, readonly) NSString *currentUsername;

/**
 The user information for the login user.
 */
@property (nonatomic, strong, readonly) DDPimaticUserInfo *userInfo;

/**
 The available devices.
 */
@property (nonatomic, strong, readonly) NSArray *devices;

/**
 The available variables.
 */
@property (nonatomic, strong, readonly) NSArray *variables;

/**
 The available groups.
 */
@property (nonatomic, strong, readonly) NSArray *groups;

/**
 The available rules.
 */
@property (nonatomic, strong, readonly) NSArray *rules;

/**
 The available pages.
 */
@property (nonatomic, strong, readonly) NSArray *pages;


/**
 Login to the pimatic server with user credentials.
 @param username   The username.
 @param password   The password
 @param completion The completion callback.
 */
- (void) loginWithUsername:(NSString*)username password:(NSString*)password completion:(DDPimaticCompletionHandler)completion;


/**
 Logout from the pimatic server.
 @param completion The completion callback.
 */
- (void) logoutWithCompletion:(DDPimaticCompletionHandler)completion;


/**
 Execute an action of a device.
 
 @param action     The device action.
 @param uid        The uid of the device.
 @param params     Optional parameters of the action.
 @param completion The completion handler to call. If the action has returns, result is set.
 */
- (void) executeDeviceAction:(DDPimaticDeviceAction*)action deviceUID:(NSString*)uid params:(NSDictionary*)params completion:(DDPimaticCompletionHandler)completion;





@end
