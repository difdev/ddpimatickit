//
//  DDPimaticUserPermissions.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 23.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


extern NSString* const DDPimaticPermissionNone;
extern NSString* const DDPimaticPermissionRead;
extern NSString* const DDPimaticPermissionWrite;


/**
 Describes the permissions of the login user.
 */
@interface DDPimaticUserPermissions : DDPimaticBaseObject

@property (nonatomic, assign, readonly) BOOL  canRestart;
@property (nonatomic, assign, readonly) BOOL  canControlDevices;

@property (nonatomic, strong, readonly) NSString *permissionRules;
@property (nonatomic, strong, readonly) NSString *permissionPages;
@property (nonatomic, strong, readonly) NSString *permissionDevices;
@property (nonatomic, strong, readonly) NSString *permissionUpdates;
@property (nonatomic, strong, readonly) NSString *permissionMessages;
@property (nonatomic, strong, readonly) NSString *permissionPlugins;
@property (nonatomic, strong, readonly) NSString *permissionConfig;
@property (nonatomic, strong, readonly) NSString *permissionDatabase;
@property (nonatomic, strong, readonly) NSString *permissionVariables;
@property (nonatomic, strong, readonly) NSString *permissionGroups;
@property (nonatomic, strong, readonly) NSString *permissionEvents;

@end
