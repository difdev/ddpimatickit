//
//  DDPimaticGroup.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 28.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticGroup.h"

@implementation DDPimaticGroup

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.uid    = json[@"id"];
        self.name   = json[@"name"];
        
        self.deviceIdentifiers    = [NSArray arrayWithArray:json[@"devices"]];
        self.variableIdentifiers  = [NSArray arrayWithArray:json[@"variables"]];
        self.ruleIdentifiers      = [NSArray arrayWithArray:json[@"rules"]];
    }
    return self;
}


- (BOOL)isEqual:(id)anObject
{
    if ( self == anObject ) {
        return YES;
    }
    
    if ( ![anObject isKindOfClass:[DDPimaticGroup class]] ) {
        return NO;
    }
    
    return [self.uid isEqualToString:((DDPimaticGroup*)anObject).uid];
}

- (NSUInteger) hash
{
    return self.uid.hash;
}


- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticGroup %@ - %@", self.uid, self.name];
}

@end
