//
//  DDPimaticPage.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 17.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticPage.h"

@implementation DDPimaticPage

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.uid    = json[@"id"];
        self.name   = json[@"name"];

        NSMutableArray *identifiers = [NSMutableArray new];
        for (NSDictionary *dev in json[@"devices"]) {
            NSString *uid = dev[@"deviceId"];
            if ( uid != nil ) {
                [identifiers addObject:uid];
            }
        }
        self.deviceIdentifiers = [NSArray arrayWithArray:identifiers];
    }
    return self;
}


- (BOOL)isEqual:(id)anObject
{
    if ( self == anObject ) {
        return YES;
    }
    
    if ( ![anObject isKindOfClass:[DDPimaticPage class]] ) {
        return NO;
    }
    
    return [self.uid isEqualToString:((DDPimaticPage*)anObject).uid];
}

- (NSUInteger) hash
{
    return self.uid.hash;
}


- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticGroup %@ - %@", self.uid, self.name];
}


@end
