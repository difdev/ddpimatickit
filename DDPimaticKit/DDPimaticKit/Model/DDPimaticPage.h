//
//  DDPimaticPage.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 17.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDPimaticPage : NSObject

- (instancetype)initWithJSON:(NSDictionary*)json;

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSArray *deviceIdentifiers;

@end
