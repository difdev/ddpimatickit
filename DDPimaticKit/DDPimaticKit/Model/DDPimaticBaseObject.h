//
//  DDPimaticBaseObject.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDPimaticBaseObject : NSObject

@property (nonatomic, strong) NSDictionary *json;

- (instancetype)initWithJSON:(NSDictionary*)json;
- (id) toJSON;

@end
