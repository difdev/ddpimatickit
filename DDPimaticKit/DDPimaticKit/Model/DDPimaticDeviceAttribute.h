//
//  DDPimaticDeviceAttribute.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


@interface DDPimaticDeviceAttribute : DDPimaticBaseObject

@property (nonatomic, strong) NSString          *acronym;
@property (nonatomic, strong) NSString          *attributeDescription;
@property (nonatomic, strong) NSString          *label;
@property (nonatomic, strong) NSArray           *labels;
@property (nonatomic, strong) NSString          *name;
@property (nonatomic, strong) NSString          *type;
@property (nonatomic, strong) NSString          *unit;
@property (nonatomic, strong) id                value;
@property (nonatomic, strong) NSDate            *lastUpdate;
@property (nonatomic, strong) NSMutableArray    *history;

@property (nonatomic, assign, getter=isDiscrete) BOOL discrete;


@end
