//
//  DDPimaticVariable.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticVariable.h"

@implementation DDPimaticVariable


- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.name   = json[@"name"];
        self.type   = json[@"type"];
        self.unit   = json[@"unit"];
        self.value  = json[@"value"];
        
        NSNumber *ro = json[@"readonly"];
        self.readonly = (ro != nil) && ro.boolValue;
    }
    return self;
}


- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticVariable %@ - %@", self.name, self.value];
}

@end
