//
//  DDPimaticDevice.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticDevice.h"
#import "DDPimaticDeviceAction.h"
#import "DDPimaticDeviceAttribute.h"

@implementation DDPimaticDevice

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.uid            = json[@"id"];
        self.name           = json[@"name"];
        self.templateName   = json[@"template"];
        
        NSMutableArray *attributes = [NSMutableArray new];
        NSArray *jsonAttributes = json[@"attributes"];
        for (id jsonAttribute in jsonAttributes) {
            [attributes addObject:[[DDPimaticDeviceAttribute alloc] initWithJSON:jsonAttribute]];
        }
        self.attributes = attributes;

        NSMutableArray *actions = [NSMutableArray new];
        NSArray *jsonActions = json[@"actions"];
        for (id jsonAction in jsonActions) {
            [actions addObject:[[DDPimaticDeviceAction alloc] initWithJSON:jsonAction]];
        }
        self.actions = actions;
        
        self.config         = [NSDictionary dictionaryWithDictionary:json[@"config"]];
        self.configDefaults = [NSDictionary dictionaryWithDictionary:json[@"configDefaults"]];        
    }
    
    return self;
}

- (BOOL)isEqual:(id)anObject
{
    if ( self == anObject ) {
        return YES;
    }
    
    if ( ![anObject isKindOfClass:[DDPimaticDevice class]] ) {
        return NO;
    }
    
    return [self.uid isEqualToString:((DDPimaticDevice*)anObject).uid];
}

- (NSUInteger) hash
{
    return self.uid.hash;
}


- (NSString*) description
{
    return [NSString stringWithFormat:@"Device %@ - %@", self.uid, self.name];
}

@end
