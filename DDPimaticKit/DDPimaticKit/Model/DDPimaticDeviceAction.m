//
//  DDpimaticDeviceAction.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticDeviceAction.h"

@implementation DDPimaticDeviceAction

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.name               = json[@"name"];
        self.actionDescription  = json[@"description"];
        self.params             = json[@"params"];
        self.returns            = json[@"returns"];
    }
    return self;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"DDpimaticDeviceAction %@ - %@", self.name, self.actionDescription];
}

@end
