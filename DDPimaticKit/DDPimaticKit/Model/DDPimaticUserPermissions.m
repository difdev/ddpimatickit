//
//  DDPimaticUserPermissions.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 23.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticUserPermissions.h"


NSString* const DDPimaticPermissionNone     = @"none";
NSString* const DDPimaticPermissionRead     = @"read";
NSString* const DDPimaticPermissionWrite    = @"write";

@interface DDPimaticUserPermissions()

@property (nonatomic, assign) BOOL  canRestart;
@property (nonatomic, assign) BOOL  canControlDevices;

@property (nonatomic, strong) NSString *permissionRules;
@property (nonatomic, strong) NSString *permissionPages;
@property (nonatomic, strong) NSString *permissionDevices;
@property (nonatomic, strong) NSString *permissionUpdates;
@property (nonatomic, strong) NSString *permissionMessages;
@property (nonatomic, strong) NSString *permissionPlugins;
@property (nonatomic, strong) NSString *permissionConfig;
@property (nonatomic, strong) NSString *permissionDatabase;
@property (nonatomic, strong) NSString *permissionVariables;
@property (nonatomic, strong) NSString *permissionGroups;
@property (nonatomic, strong) NSString *permissionEvents;

@end


@implementation DDPimaticUserPermissions


- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.canRestart         = ((NSNumber*)json[@"restart"]).boolValue;
        self.canControlDevices  = ((NSNumber*)json[@"controlDevices"]).boolValue;

        self.permissionRules        = json[@"rules"];
        self.permissionPages        = json[@"pages"];
        self.permissionDevices      = json[@"devices"];
        self.permissionUpdates      = json[@"updates"];
        self.permissionMessages     = json[@"messages"];
        self.permissionPlugins      = json[@"plugins"];
        self.permissionConfig       = json[@"config"];
        self.permissionDatabase     = json[@"database"];
        self.permissionVariables    = json[@"variables"];
        self.permissionGroups       = json[@"groups"];
        self.permissionEvents       = json[@"events"];
    }
    return self;
}

- (id) toJSON
{
    NSMutableDictionary *json = [NSMutableDictionary new];
    
    json[@"restart"]        = [NSNumber numberWithBool:self.canRestart];
    json[@"controlDevices"] = [NSNumber numberWithBool:self.canControlDevices];
    
    json[@"rules"]      = self.permissionRules;
    json[@"pages"]      = self.permissionPages;
    json[@"devices"]    = self.permissionDevices;
    json[@"updates"]    = self.permissionUpdates;
    json[@"messages"]   = self.permissionMessages;
    json[@"plugins"]    = self.permissionPlugins;
    json[@"config"]     = self.permissionConfig;
    json[@"database"]   = self.permissionDatabase;
    json[@"variables"]  = self.permissionVariables;
    json[@"groups"]     = self.permissionGroups;
    json[@"events"]     = self.permissionEvents;
    
    return json;
}


@end
