//
//  DDPimaticDeviceAttribute.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticCategories.h"
#import "DDPimaticDeviceAttribute.h"
#import "DDPimaticHistoryItem.h"

@implementation DDPimaticDeviceAttribute

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.acronym                = json[@"acronym"];
        self.attributeDescription   = json[@"description"];
        self.label                  = json[@"label"];
        self.labels                 = [NSArray arrayWithArray:json[@"labels"]];
        self.name                   = json[@"name"];
        self.type                   = json[@"type"];
        self.unit                   = json[@"unit"];
        self.value                  = json[@"value"];
        self.discrete               = ((NSNumber*)json[@"discrete"]).boolValue;
        self.lastUpdate             = [NSDate dateWithPimaticDate:json[@"lastUpdate"]];
        
        NSMutableArray *history = [NSMutableArray new];
        NSArray *jsonHistory = json[@"history"];
        for (id jsonItem in jsonHistory) {
            [history addObject:[[DDPimaticHistoryItem alloc] initWithJSON:jsonItem]];
        }
        self.history = history;
    }
    return self;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticDeviceAttribute %@ (%@): %@ %@", self.name, self.type, self.value, self.unit];
}

@end
