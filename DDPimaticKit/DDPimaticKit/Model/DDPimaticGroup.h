//
//  DDPimaticGroup.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 28.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


@interface DDPimaticGroup : DDPimaticBaseObject

- (instancetype)initWithJSON:(NSDictionary*)json;

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSArray *deviceIdentifiers;
@property (nonatomic, strong) NSArray *variableIdentifiers;
@property (nonatomic, strong) NSArray *ruleIdentifiers;


@end
