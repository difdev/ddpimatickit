//
//  DDPimaticVariable.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


@interface DDPimaticVariable : DDPimaticBaseObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) id       value;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, assign, getter=isReadonly) BOOL readonly;

@end
