//
//  DDPimaticHistoryItem.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 03.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticCategories.h"
#import "DDPimaticHistoryItem.h"

@implementation DDPimaticHistoryItem

+ (instancetype) historyItemWithDate:(NSDate*)date value:(id)value
{
    DDPimaticHistoryItem *item = [DDPimaticHistoryItem new];
    item.date   = date;
    item.value  = value;
    
    return item;
}

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.date   = [NSDate dateWithPimaticDate:json[@"t"]];
        self.value  = json[@"v"];
    }
    return self;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticHistoryItem %@ - %@", self.date, self.value];
}

@end
