//
//  DDPimaticUserInfo.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 23.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


@class DDPimaticUserPermissions;

/**
 Delivers informations about the login user.
 */
@interface DDPimaticUserInfo : DDPimaticBaseObject

@property (nonatomic, strong, readonly) NSString *username;
@property (nonatomic, strong, readonly) NSString *role;

@property (nonatomic, strong, readonly) DDPimaticUserPermissions *permissions;

@end
