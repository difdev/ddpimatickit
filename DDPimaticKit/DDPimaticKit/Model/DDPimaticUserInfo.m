//
//  DDPimaticUserInfo.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 23.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticUserInfo.h"
#import "DDPimaticUserPermissions.h"


@interface DDPimaticUserInfo()

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) DDPimaticUserPermissions *permissions;

@end


@implementation DDPimaticUserInfo

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.username       = json[@"username"];
        self.role           = json[@"role"];
        self.permissions    = [[DDPimaticUserPermissions alloc] initWithJSON:json[@"permissions"]];
    }
    return self;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticUserInfo %@ - %@", self.username, self.role];
}

@end
