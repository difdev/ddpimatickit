//
//  DDPimaticRule.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticRule.h"

@implementation DDPimaticRule

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.uid    = json[@"id"];
        self.name   = json[@"name"];
        
        self.active     = ((NSNumber*)json[@"active"]).boolValue;
        self.valid      = ((NSNumber*)json[@"valid"]).boolValue;
        self.logging    = ((NSNumber*)json[@"logging"]).boolValue;

        self.conditionToken = json[@"conditionToken"];
        self.actionsToken   = json[@"actionsToken"];
        self.tokenString    = json[@"string"];
        self.error          = json[@"error"];
    }
    return self;
}


- (BOOL)isEqual:(id)anObject
{
    if ( self == anObject ) {
        return YES;
    }
    
    if ( ![anObject isKindOfClass:[DDPimaticRule class]] ) {
        return NO;
    }
    
    return [self.uid isEqualToString:((DDPimaticRule*)anObject).uid];
}

- (NSUInteger) hash
{
    return self.uid.hash;
}


- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticRule %@ - %@", self.uid, self.name];
}

@end
