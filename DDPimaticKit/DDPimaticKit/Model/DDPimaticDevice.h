//
//  DDPimaticDevice.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"

@interface DDPimaticDevice : DDPimaticBaseObject

@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *templateName;

@property (nonatomic, strong) NSMutableArray *attributes;
@property (nonatomic, strong) NSMutableArray *actions;

@property (nonatomic, strong) NSDictionary *config;
@property (nonatomic, strong) NSDictionary *configDefaults;

@end

