//
//  DDPimaticHistoryItem.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 03.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


@interface DDPimaticHistoryItem : DDPimaticBaseObject

+ (instancetype) historyItemWithDate:(NSDate*)date value:(id)value;

@property (nonatomic, strong) NSDate   *date;
@property (nonatomic, strong) id       value;

@end
