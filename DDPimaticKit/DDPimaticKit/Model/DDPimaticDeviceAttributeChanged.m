//
//  DDPimaticDeviceAttributeChanged.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 27.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticCategories.h"
#import "DDPimaticDeviceAttributeChanged.h"

@interface DDPimaticDeviceAttributeChanged()

@property (nonatomic, strong) NSString  *attributeName;
@property (nonatomic, strong) NSString  *deviceId;
@property (nonatomic, strong) id        value;
@property (nonatomic, strong) NSDate    *time;

@end


@implementation DDPimaticDeviceAttributeChanged

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.attributeName  = json[@"attributeName"];
        self.deviceId       = json[@"deviceId"];
        self.value          = json[@"value"];
        self.time           = [NSDate dateWithPimaticDate:json[@"time"]];
    }
    return self;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"DDPimaticDeviceAttributeChanged (%@) %@ - %@ : %@", self.time, self.deviceId, self.attributeName, self.value];
}

@end
