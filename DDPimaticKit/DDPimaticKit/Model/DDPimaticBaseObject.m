//
//  DDPimaticBaseObject.m
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 12.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import "DDPimaticBaseObject.h"

@implementation DDPimaticBaseObject

- (instancetype)initWithJSON:(NSDictionary*)json
{
    self = [super init];
    if (self) {
        self.json = json;
    }
    return self;
}

- (id) toJSON
{
    return nil;
}

@end
