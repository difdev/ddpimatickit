//
//  DDpimaticDeviceAction.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 01.05.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


@interface DDPimaticDeviceAction : DDPimaticBaseObject

@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSString      *actionDescription;
@property (nonatomic, strong) NSDictionary  *params;
@property (nonatomic, strong) NSDictionary  *returns;

@end
