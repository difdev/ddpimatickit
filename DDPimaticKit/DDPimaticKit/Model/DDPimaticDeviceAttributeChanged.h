//
//  DDPimaticDeviceAttributeChanged.h
//  DDPimaticKit
//
//  Created by Dirk Fischbach on 27.04.15.
//  Copyright (c) 2015 difdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDPimaticBaseObject.h"


@interface DDPimaticDeviceAttributeChanged : DDPimaticBaseObject

@property (nonatomic, strong, readonly) NSString  *attributeName;
@property (nonatomic, strong, readonly) NSString  *deviceId;
@property (nonatomic, strong, readonly) id        value;
@property (nonatomic, strong, readonly) NSDate    *time;

@end
